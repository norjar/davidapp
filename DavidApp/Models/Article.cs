﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DavidApp.Models
{
    public class Article
    {
        public string Content { get; set; }
        public List<string> Split = new List<string>();
    }
}