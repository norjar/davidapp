using System.Linq;
using Microsoft.AspNetCore.Mvc;
using DavidApp2.Models;
using DavidApp2.Services;

namespace DavidApp2.Controllers
{
    public class ArticleController : Controller
    {
        private readonly IContentService contentService;

   

        public ArticleController(IContentService contentService)
        {
            this.contentService = contentService;
        }

        // GET: Article/Content
        public ActionResult Index()
        {
            var article = contentService.GetContent();


            article.Split = article.Content.Split(' ').ToList<string>();

            return View(article);






        }
    }
}